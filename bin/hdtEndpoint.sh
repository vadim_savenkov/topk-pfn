#!/bin/bash

if [ $HDTTOPK_DEBUG_PORT ]; then DEBUGSTR="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=$HDTTOPK_DEBUG_PORT"; fi

if [ $HDTTOPK_MXRAM_MB ]; then XMXSTR="-Xmx${HDTTOPK_MXRAM_MB}m"; fi


java -cp "lib/*" $XMXSTR $DEBUGSTR  "org.rdfhdt.hdt.fuseki.FusekiHDTCmd"  "$@"
