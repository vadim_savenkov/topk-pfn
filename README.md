To deploy simply call 

  `mvn install`

You can run the code from the ./target directory:

  `cd target`

#### For SPARQL query 

`bin/hdtsparql.sh {path/to/hdt/file} {query}`

For instance: 

`bin/hdtsparql.sh data/sample.hdt "PREFIX : <http://dbpedia.org/resource/> PREFIX ppf:<java:at.ac.wu.arqext.path.> SELECT * WHERE{ ?x :knows ?y . ?y :knows ?z . ?path ppf:topk (?x ?y 2) }"`

#### For Fuseki

`bin/hdtEndpoint.sh --hdt {path/to/hdt/file} /{dataset-name}`

For instance:

`bin/hdtEndpoint.sh --hdt data/sample.hdt /ds`