package at.ac.wu.arqext.path;

import at.ac.wu.graphsense.Edge;
import at.ac.wu.graphsense.GraphIndex;
import at.ac.wu.graphsense.Util;
import at.ac.wu.graphsense.hdt.HDTIntGraphIndex;
import at.ac.wu.graphsense.hdt.HDTPathExprFactory;
import at.ac.wu.graphsense.hdt.HDTRegExpPathArbiter;
import at.ac.wu.graphsense.rdf.RDFGraphIndex;
import at.ac.wu.graphsense.rdf.RDFPathExprFactory;
import at.ac.wu.graphsense.search.BidirectionalTopK;
import at.ac.wu.graphsense.search.SearchProgressListener;
import at.ac.wu.graphsense.search.patheval.PathArbiter;
import at.ac.wu.graphsense.search.patheval.RegExpPathArbiter;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryParseException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.ExecutionContext;
import org.apache.jena.sparql.engine.QueryIterator;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import org.apache.jena.sparql.path.Path;
import org.apache.jena.sparql.path.PathParser;
import org.apache.jena.sparql.pfunction.PFuncSimpleAndList;
import org.apache.jena.sparql.pfunction.PropFuncArg;
import org.apache.jena.atlas.logging.Log;
import org.apache.jena.sparql.util.IterLib;
import org.apache.jena.sparql.util.Symbol;
import org.rdfhdt.hdtjena.HDTGraph;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Vadim on 02.06.2017.
 */
public class topk extends PFuncSimpleAndList {

    final public static String URI = PathPropertyFunctionFactory.NAMESPACE+"topk";

    enum Argument {SOURCE, TARGET, K, PATHEXPR, MAXLEN, TIMEOUTMS };

    @Override
    public QueryIterator execEvaluated(Binding binding, Node subject, Node predicate, PropFuncArg object, ExecutionContext execCxt) {

            Var pathVar;
            String[] st = new String[2];
            List<Set<Node>> nst = new LinkedList<>();
            nst.add(new HashSet<Node>());
            nst.add(new HashSet<Node>());

            int k = 1;
            int maxlen = Integer.MAX_VALUE;
            int timeoutms = -1;
            Path path = null;

            if (object.getArgListSize() < 2) {
                Log.warn(this, "The predicate function expects at least two arguments (start node, end node)");
                return IterLib.noResults(execCxt);
            }


            if (!subject.isVariable()) {
                Log.warn(this, "Value found in the subject position: expected a variable: " + subject);
                return IterLib.noResults(execCxt);
            }
            else{
                pathVar = Var.alloc(subject);
            }

            if( object.getArg(0).isLiteral() && object.getArg(0).getLiteralValue().toString().startsWith("-") ){
                // named argument mode

                Argument waitFor = Argument.SOURCE;

                for(int i=0; i<object.getArgListSize(); i++){
                    Node n = object.getArg(i);

                    if( n.isLiteral() && n.getLiteralValue().toString().startsWith("--source") ){
                        waitFor = Argument.SOURCE;
                        continue;
                    }
                    if( n.isLiteral() && n.getLiteralValue().toString().startsWith("--target") ){
                        waitFor = Argument.TARGET;
                        continue;
                    }
                    if( n.isLiteral() && n.getLiteralValue().toString().startsWith("--pattern") ){
                        waitFor = Argument.PATHEXPR;
                        continue;
                    }
                    if( n.isLiteral() && n.getLiteralValue().toString().startsWith("--k") ){
                        waitFor = Argument.K;
                        continue;
                    }
                    if( n.isLiteral() && n.getLiteralValue().toString().startsWith("--maxlength") ){
                        waitFor = Argument.MAXLEN;
                        continue;
                    }
                    if( n.isLiteral() && n.getLiteralValue().toString().startsWith("--timeout") ){
                        waitFor = Argument.TIMEOUTMS;
                        continue;
                    }

                    switch (waitFor){
                        case SOURCE:
                            nst.get(0).add(object.getArg(i));
                            break;
                        case TARGET:
                            nst.get(1).add(object.getArg(i));
                            break;
                        case K:
                            try {
                                k = Integer.parseInt(object.getArg(i).getLiteralValue().toString());
                            } catch (Exception e) {
                                Log.warn(this, "Could not parse the argument --k of the topk function: integer (number of paths k) expected");
                                return IterLib.noResults(execCxt);
                            }
                            break;
                        case PATHEXPR:
                            try {
                                String pathExpr = object.getArg(i).getLiteralValue().toString();

                                Symbol smbQuery = Symbol.create("http://jena.apache.org/ARQ/system#query");
                                Query q = (Query)execCxt.getContext().get(smbQuery);
                                Model parseModel = ModelFactory.createDefaultModel();
                                if( q.getPrefixMapping()!=null ) {
                                    parseModel.setNsPrefixes(q.getPrefixMapping());
                                }
                                path = PathParser.parse(pathExpr, parseModel);
                            }
                            catch (QueryParseException e){
                                Log.warn(this, "Error parsing path expression --pattern in the topk function: "+e.getMessage());
                                return IterLib.noResults(execCxt);
                            }
                            catch (Exception e) {
                                Log.warn(this, "Could not parse the argument --pattern of the topk function: string representing a valid path expression expected");
                                return IterLib.noResults(execCxt);
                            }
                            break;
                        case MAXLEN:
                            try {
                                maxlen = Integer.parseInt(object.getArg(i).getLiteralValue().toString());
                            } catch (Exception e) {
                                Log.warn(this, "Could not parse the argument --maxlength of the topk function: integer (maximum path length) expected");
                                return IterLib.noResults(execCxt);
                            }
                            break;
                        case TIMEOUTMS:
                            try {
                                timeoutms = Integer.parseInt(object.getArg(i).getLiteralValue().toString());
                            } catch (Exception e) {
                                Log.warn(this, "Could not parse the argument --timeout of the topk function: integer (duration in milliseconds) expected");
                                return IterLib.noResults(execCxt);
                            }
                            break;
                    }
                }

            }
            else { //implicit arguments: single source & single target

                for( int i = 0; i<2; i++ ){
                    if( Var.isVar(object.getArg(i)) ){
                        final String card[] = {"first", "second"};
                        Log.warn(this,"The "+card[i]+" argument of the topk function must be bound");
                        return IterLib.noResults(execCxt);
                    }
                    else{
                        nst.get(i).add( object.getArg(i) );
                    }
                }

                if( object.getArgListSize() > 2 ){
                    try {
                        k = Integer.parseInt(object.getArg(2).getLiteralValue().toString());
                    } catch (Exception e) {
                        Log.warn(this, "Could not parse the third argument of the topk function: integer (number of paths k) expected");
                        return IterLib.noResults(execCxt);
                    }
                }

                if( object.getArgListSize() > 3 ){
                    try {
                        String pathExpr = object.getArg(3).getLiteralValue().toString();

                        Symbol smbQuery = Symbol.create("http://jena.apache.org/ARQ/system#query");
                        Query q = (Query)execCxt.getContext().get(smbQuery);
                        Model parseModel = ModelFactory.createDefaultModel();
                        if( q.getPrefixMapping()!=null ) {
                            parseModel.setNsPrefixes(q.getPrefixMapping());
                        }
                        path = PathParser.parse(pathExpr, parseModel);
                    }
                    catch (QueryParseException e){
                        Log.warn(this, "Error parsing pathe expression in the topk function: "+e.getMessage());
                        return IterLib.noResults(execCxt);
                    }
                    catch (Exception e) {
                        Log.warn(this, "Could not parse the fourth argument of the topk function: string representing a valid path expression expected");
                        return IterLib.noResults(execCxt);
                    }
                }

            }

            Graph dataGraph = execCxt.getActiveGraph();
/*
            boolean noHDT = false;
            if(System.getProperty("nohdt")!=null && System.getProperty("nohdt").equals("true")){
                noHDT = true;
            }

            if(!noHDT && dataGraph instanceof HDTGraph){

                HDTIntGraphIndex gi = new HDTIntGraphIndex(((HDTGraph)dataGraph).getHDT());

                int source = gi.vertexKey(st[0], Edge.Component.SOURCE);
                int target = gi.vertexKey(st[1], Edge.Component.TARGET);

                if( source < 0 ){
                    Log.warn(this, "topk error: source node not found in the HDT graph");
                    return IterLib.noResults(execCxt);
                }
                if( target < 0 ){
                    Log.warn(this, "topk error: target node not found in the HDT graph");
                    return IterLib.noResults(execCxt);
                }

                BidirectionalTopK<Integer,Integer> topK = new BidirectionalTopK<>(gi);

                if( path!=null ){
                    PathArbiter<Integer,Integer> parb =
                            new HDTRegExpPathArbiter(HDTPathExprFactory.createPathExpr(path,gi));
                    topK.setPathArbiter(parb);
                }

                Collection<List<Edge<Integer,Integer>>> results;

                results = topK.run(source, target, k);

                return new QueryIterPlainWrapper(
                        new BindingIterator<>(binding,pathVar,results.iterator(),gi));
            }
*/
            RDFGraphIndex gi = new RDFGraphIndex(dataGraph);

            BidirectionalTopK<Node,Node> topK = new BidirectionalTopK<>(gi);

            if( path!=null ){
                PathArbiter<Node,Node> parb =
                        new RegExpPathArbiter<>(RDFPathExprFactory.createPathExpr(path));
                topK.setPathArbiter(parb);
            }

            if( maxlen < Integer.MAX_VALUE ){
                topK.addProgressListener( new StepCounter<Node, Node>(maxlen) );
            }
            if(timeoutms > 0){
                topK.addProgressListener( new TimeoutChecker<Node, Node>(timeoutms));
            }

            Collection<List<Edge<Node,Node>>> results = Collections.emptyList();

            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<Collection<List<Edge<Node,Node>>>> future = executor.submit(
                    new TopKTask( topK, nst.get(0), nst.get(1), k ) );

            try {
                results = timeoutms > 0 ? future.get(timeoutms, TimeUnit.MILLISECONDS) : future.get();
            }
            catch(ExecutionException ex){

            } catch (InterruptedException e) {

            } catch (TimeoutException e) {

            }

        return new QueryIterPlainWrapper(
                    new BindingIterator<>(binding,pathVar,results.iterator(),gi));

    }


    class TopKTask implements Callable<Collection<List<Edge<Node, Node>>>>{

        BidirectionalTopK<Node,Node> topk;
        Collection<Node> source, target;
        int k;

        TopKTask(BidirectionalTopK<Node,Node> topk, Collection<Node> source, Collection<Node> target, int k){
            this.topk = topk;
            this.source = source;
            this.target = target;
            this.k = k;
        }

        @Override
        public Collection<List<Edge<Node, Node>>> call() throws Exception {
            return topk.run(source, target, k);
        }

    }

    public static class BindingIterator<V,E> implements Iterator<Binding>
    {
        Binding inputBinding;
        Var pathVar;
        Iterator<List<Edge<V,E>>> inner;
        List<Var> allVars;
        GraphIndex<V,E> gi;

        public BindingIterator(Binding bd, Var pathVar, Iterator<List<Edge<V,E>>> inner, GraphIndex<V,E> gi){

            this.inputBinding = bd;
            this.pathVar = pathVar;
            this.allVars = new LinkedList<>();
            Iterator<Var> vit = bd.vars();
            while( vit.hasNext() ) {
                allVars.add(vit.next());
            }
            allVars.add(pathVar);

            this.inner = inner;
            this.gi = gi;
        }


        @Override
        public boolean hasNext() {
            return inner!=null && inner.hasNext();
        }

        @Override
        public Binding next() {
            return new ComposedBinding(inner.next());
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove operation not supported");
        }



        public class ComposedBinding implements Binding{
            List<Edge<V,E>> path;

            public ComposedBinding(List<Edge<V,E>> path){
                this.path = path;
            }

            @Override
            public Iterator<Var> vars() {
                return allVars.iterator();
            }

            @Override
            public boolean contains(Var var) {
                return allVars.contains(var);
            }

            @Override
            public Node get(Var var) {
                if( pathVar.equals(var) ) {
                    return NodeFactory.createLiteral(Util.format(path, gi));
                }
                return inputBinding.get(var);
            }

            //size of the Binding
            @Override
            public int size() {
                return allVars.size();
            }

            //Whether the binding is empty
            @Override
            public boolean isEmpty() {
                return false;
            }
        }

    }


    static class StepCounter<V,E> implements SearchProgressListener<V,E>{

        int maxSteps = Integer.MAX_VALUE;
        int step = 0;

        StepCounter(int maxSteps){
            this.maxSteps = maxSteps;
        }

        @Override
        public boolean onAdvance(int numVertices, int numEdges, int direction) {
            if( direction == 1 ){
                step++;
            }
            return step < maxSteps;
        }

        @Override
        public boolean onPathFound(List path) {
            return true;
        }

    }

    static class TimeoutChecker<V,E> implements SearchProgressListener<V,E>{

        long timeout = Long.MAX_VALUE;

        long start = System.currentTimeMillis();

        TimeoutChecker(int timeout){
            this.timeout = (long) timeout;
        }

        @Override
        public boolean onAdvance(int numVertices, int numEdges, int direction) {
            return System.currentTimeMillis() - start >= timeout;
        }

        @Override
        public boolean onPathFound(List path) {
            return true;
        }

    }
}
