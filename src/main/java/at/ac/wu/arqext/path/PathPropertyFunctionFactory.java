package at.ac.wu.arqext.path;
import org.apache.jena.sparql.pfunction.PropertyFunction;
import org.apache.jena.sparql.pfunction.PropertyFunctionFactory;

/**
 * Created by Vadim on 02.06.2017.
 */
public class PathPropertyFunctionFactory implements PropertyFunctionFactory {

    public final static String NAMESPACE = "http://wu.ac.at/sparql/pathfn#";

    @Override
    public PropertyFunction create(final String uri)
    {
        switch (uri) {
            case topk.URI:
            return new topk();
        }
        return null;
    }
}
