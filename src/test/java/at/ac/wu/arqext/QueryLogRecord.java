package at.ac.wu.arqext;

import at.ac.wu.graphsense.Edge;
import at.ac.wu.graphsense.Util;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.spring.TableCreator;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.j256.ormlite.table.DatabaseTableConfigLoader;
import com.j256.ormlite.table.TableInfo;
import com.j256.ormlite.table.TableUtils;
import org.apache.jena.graph.Node;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.j256.ormlite.field.DataType.LONG_STRING;
import static junit.framework.TestCase.fail;

/**
 * Created by vadim on 08.06.17.
 */
public class QueryLogRecord {

    public static Dao<QueryLogRecord, String> dao;

    long creationTime;

    @DatabaseField(id = true, width = 2048)
    String id;

    @DatabaseField
    Date ts;

    @DatabaseField
    String experiment;

    @DatabaseField
    Integer desiredLength;

    @DatabaseField
    String hostname;

    @DatabaseField
    String dataset;

    @DatabaseField
    String queryId;

    @DatabaseField
    Long runtimeMillis;

    //@DatabaseField(dataType = LONG_STRING)
    String queryText;

    @DatabaseField
    Boolean edgeInt;

    @DatabaseField(dataType = LONG_STRING)
    String pathExpr;

    @DatabaseField
    Integer minPathLength;

    @DatabaseField
    Integer maxPathLength;

    @DatabaseField(dataType = LONG_STRING)
    String shortestPath;

    @DatabaseField(dataType = LONG_STRING)
    String kthShortestPath;

    @DatabaseField
    String source;

    @DatabaseField
    String target;

    @DatabaseField
    Integer k;

    @DatabaseField
    Integer pathsFound;

    @DatabaseField(dataType = LONG_STRING)
    String ppPrototypePath;

    PathStatistics<Node,Node> ps;

    @DatabaseField
    Integer ppLength;

    @DatabaseField
    Boolean ppUniqueResources;

    @DatabaseField
    Float ppAvgInDegree;

    @DatabaseField
    Float ppAvgOutDegree;

    @DatabaseField
    Float ppMedianInDegree;

    @DatabaseField
    Float ppMedianOutDegree;

    public QueryLogRecord(){
        resetCreationTime();
    }

    public void resetCreationTime(){
        creationTime = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }


    public String getExperiment() {
        return experiment;
    }

    public void setExperiment(String experiment) {
        this.experiment = experiment;
    }

    public Integer getDesiredLength() {
        return desiredLength;
    }

    public void setDesiredLength(Integer desiredLength) {
        this.desiredLength = desiredLength;
    }


    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
        computeId();
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
        computeId();
    }

    public Long getRuntimeMillis() {
        return runtimeMillis;
    }

    public void setRuntimeMillis(Long runtimeMillis) {
        this.runtimeMillis = runtimeMillis;
    }

    public String getQueryText() {
        return queryText;
    }

    public void setQueryText(String queryText) {

        this.queryText = queryText;

        final Pattern pattern = Pattern.compile(":topk\\s*\\(.*\\\"(.*)\\\".*\\)");
        Matcher m = pattern.matcher(queryText);

        if( m.find() ){
            this.setPathExpr(m.group(1));
        }
    }

    public Boolean getEdgeInt() {
        return edgeInt;
    }

    public void setEdgeInt(Boolean edgeInt) {
        this.edgeInt = edgeInt;
    }


    public String getPathExpr() {
        return pathExpr;
    }

    public void setPathExpr(String pathExpr)
    {
        this.pathExpr = pathExpr;
        computeId();
    }

    public Integer getMinPathLength() {
        return minPathLength;
    }

    public void setMinPathLength(Integer minPathLength) {
        this.minPathLength = minPathLength;
    }

    public Integer getMaxPathLength() {
        return maxPathLength;
    }

    public void setMaxPathLength(Integer maxPathLength) {
        this.maxPathLength = maxPathLength;
    }

    public String getShortestPath() {
        return shortestPath;
    }

    public void setShortestPath(String shortestPath) {
        this.shortestPath = shortestPath!=null? shortenURIs(shortestPath) : null;
    }

    public String getKthShortestPath() {
        return kthShortestPath;
    }

    public void setKthShortestPath(String kthShortestPath) {

        this.kthShortestPath = kthShortestPath!=null? shortenURIs(kthShortestPath) : null;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {

        this.source = shortenURIs(source);
        computeId();
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {

        this.target = shortenURIs(target);
        computeId();
    }

    public Integer getK() {
        return k;
    }

    public void setK(Integer k) {

        this.k = k;
        computeId();
    }

    public Integer getPathsFound() {
        return pathsFound;
    }

    public void setPathsFound(Integer pathsFound) {
        this.pathsFound = pathsFound;
    }

    public PathStatistics<Node, Node> getPathStatistics() {
        return ps;
    }

    public void setPathStatistics(PathStatistics<Node,Node> ps) {
        this.ps = ps;
        List<Edge<Node,Node>> path = ps.path;
        String strPath = shortenURIs(Util.format(path));
        setPpPrototypePath(strPath);
        setPpLength(ps.length());
        setPpAvgInDegree( PathStatistics.avg(ps.getIndegrees()) );
        setPpAvgOutDegree( PathStatistics.avg(ps.getOutdegrees()) );
        setPpMedianInDegree( PathStatistics.median(ps.getIndegrees()) );
        setPpMedianOutDegree( PathStatistics.median(ps.getOutdegrees()) );

    }

    public static String shortenURIs(String text){
        text = text.replace("http://dbpedia.org/resource/","dbr:");
        text = text.replace("http://dbpedia.org/property/","dbp:");
        text = text.replace("http://dbpedia.org/ontology/","dbo:");
        text = text.replace("http://www.w3.org/2002/07/owl#","owl:");
        text = text.replace("http://www.w3.org/1999/02/22-rdf-syntax-ns#type","a");
        return text;
    }

    public static String expandURI( String uri ){
        if( uri.startsWith("dbr:") ){
            return "<http://dbpedia.org/resource/"+ uri.substring(4)+">";
        }
        if( uri.startsWith("dbp:") ){
            return "<http://dbpedia.org/property/"+ uri.substring(4)+">";
        }
        if( uri.startsWith("dbo:") ){
            return "<http://dbpedia.org/ontology/"+ uri.substring(4)+">";
        }
        if( uri.equals("a") ){
            return "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";
        }
        return uri;
    }

    public String getPpPrototypePath() {
        return ppPrototypePath;
    }

    public void setPpPrototypePath(String ppPrototypePath) {
        this.ppPrototypePath = ppPrototypePath;
    }

    public Integer getPpLength() {
        return ppLength;
    }

    public void setPpLength(Integer ppLength) {
        this.ppLength = ppLength;
    }

    public Boolean getPpUniqueResources() {
        return ppUniqueResources;
    }

    public void setPpUniqueResources(Boolean ppUniqueResources) {
        this.ppUniqueResources = ppUniqueResources;
    }


    public Float getPpAvgInDegree() {
        return ppAvgInDegree;
    }

    public void setPpAvgInDegree(Float ppAvgInDegree) {
        this.ppAvgInDegree = ppAvgInDegree;
    }

    public Float getPpAvgOutDegree() {
        return ppAvgOutDegree;
    }

    public void setPpAvgOutDegree(Float ppAvgOutDegree) {
        this.ppAvgOutDegree = ppAvgOutDegree;
    }

    public Float getPpMedianInDegree() {
        return ppMedianInDegree;
    }

    public void setPpMedianInDegree(Float ppMedianInDegree) {
        this.ppMedianInDegree = ppMedianInDegree;
    }

    public Float getPpMedianOutDegree() {
        return ppMedianOutDegree;
    }

    public void setPpMedianOutDegree(Float ppMedianOutDegree) {
        this.ppMedianOutDegree = ppMedianOutDegree;
    }

    void computeId(){
        id = source + ":" + target + ":" + k + ":" + pathExpr + ":" + dataset + ":" + hostname;
    }

    public static void setup(){
        try {
            String databaseUrl = "jdbc:hsqldb:file:./data/log";

            ConnectionSource cns =  new JdbcConnectionSource(databaseUrl,"sa","");

            dao = DaoManager.createDao(cns, QueryLogRecord.class);

            List<String> statements = TableUtils.
                    getCreateTableStatements(cns, QueryLogRecord.class);

            DatabaseConnection cn = cns.getReadWriteConnection(QueryLogRecord.class.toString());
            cn.setAutoCommit(true);

            for(Iterator<String> it = statements.iterator(); it.hasNext();  ){
                String stmt = it.next();

                if( stmt.matches(".*CREATE\\s+TABLE.*") ){
                    stmt = stmt.replaceAll("CREATE\\s+TABLE", "CREATE TEXT TABLE");

                    if( !stmt.matches("IF\\s+NOT\\s+EXISTS")  ){
                        stmt = stmt.replace("TABLE", "TABLE IF NOT EXISTS");
                    }
                }
                cn.executeStatement(stmt, DatabaseConnection.DEFAULT_RESULT_FLAGS);
            }

            DatabaseTableConfig cfg = DatabaseTableConfig.fromClass(cns, QueryLogRecord.class);

            cn.executeStatement("SET TABLE "+ cfg.getTableName() + " SOURCE "
                                  +"\"log.csv;all_quoted=true;ignore_first=true;fs=|\""
                                ,DatabaseConnection.DEFAULT_RESULT_FLAGS);

            TableInfo ti = ((BaseDaoImpl)dao).getTableInfo();
            FieldType[] fts = ti.getFieldTypes();

            StringBuilder sb = new StringBuilder("set table QueryLogRecord source header '");

            for(int i=0; i<fts.length; i++){
                if( i>0 ){
                    sb.append('|');
                }
                sb.append(fts[i].getColumnName());
            }
            sb.append("'");

            try {
                cn.executeStatement(sb.toString(), DatabaseConnection.DEFAULT_RESULT_FLAGS);
            }
            catch(Exception e){

            }
            cn.setAutoCommit(true);
        }
        catch(SQLException ex){
            ex.printStackTrace();
            fail();
        }
    }

    public static String lookupHostname(){
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException ex)
        {
            System.out.println("Hostname can not be resolved");
        }
        return "";
    }

    public static void shutdown(){
        try {
            dao.clearObjectCache();
            dao.getConnectionSource().close();
        }
        catch(Exception e){}
    }

    public static void write ( QueryLogRecord r ){ write(r,true); }

    public static void write( QueryLogRecord r, boolean updateRuntime ){

        if( updateRuntime ) {
            r.setRuntimeMillis(System.currentTimeMillis() - r.creationTime);
        }

        if( r.getHostname()==null || r.getHostname().isEmpty()){
            r.setHostname(lookupHostname());
        }
        if( r.getTs()==null ){
            r.setTs(new Date());
        }

        // persist the account object to the database
        try {
            dao.createOrUpdate(r);
        }
        catch(Exception ex){
            System.err.println(r.getPpPrototypePath());
            ex.printStackTrace();
        }

    }
}
