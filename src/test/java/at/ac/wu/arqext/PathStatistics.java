package at.ac.wu.arqext;

import at.ac.wu.graphsense.Edge;
import at.ac.wu.graphsense.GraphIndex;
import org.rdfhdt.hdt.iterator.utils.Iter;

import java.util.*;

/**
 * Created by vadim on 09.06.17.
 */
public class PathStatistics<V,E> {

    List<Edge<V,E>> path;
    List<Integer> outdegrees;
    List<Integer> indegrees;

    public PathStatistics(List<Edge<V,E>> path){

        this.path = path;

    }

    public void computeIndegrees(GraphIndex<V,E> gi){
        indegrees = new LinkedList<>();
        if(path==null || path.isEmpty() ){
            return;
        }
        for(Edge<V,E> e : path){
            if(e == path.get(0)){
                //for indegrees, do not consider the first vertex
                continue;
            }
            Iterator<Edge<V,E>> it = gi.lookupEdges(null, e.vertex());
            indegrees.add(iteratorSize(it));
        }
    }

    public static int iteratorSize(Iterator it){
        int n = 0;
        while(it.hasNext()){
            it.next();
            n++;
        }
        return n;
    }

    public static int lengthFromStringTrace(String path){
        int numLts = 0;
        for( int i =0; i<path.length(); i++ ){
            if(path.charAt(i)=='<'){
                numLts++;
            }
        }
        return numLts;
    }

    public static Float avg(Collection<? extends Number> coll){
        if(coll==null || coll.isEmpty()) {
            return null;
        }
        float sum = 0;
        for (Number val : coll) {
            sum += val.floatValue();
        }
        return sum / coll.size();
    }

    public static Float median( Collection<? extends Number> coll ){
        if( coll==null || coll.isEmpty() ){
            return null;
        }
        if( coll.size() < 3 ){
            return coll.iterator().next().floatValue();
        }

        List<Float> sorted = new LinkedList<>();
        for( Number n : coll ){
            sorted.add(n.floatValue());
        }
        Collections.sort(sorted);

        return (sorted.get(sorted.size()/2) + sorted.get(sorted.size()/2 - 1))/2;
    }

    public List<Edge<V, E>> getPath() {
        return path;
    }

    public int length(){
        if( path==null || path.isEmpty() ){
            return 0;
        }
        return path.size()-1;
    }

    public List<Integer> getOutdegrees() {
        return outdegrees;
    }

    public void setOutdegrees(List<Integer> outdegrees) {
        this.outdegrees = outdegrees;
    }

    public List<Integer> getIndegrees() {
        return indegrees;
    }

    public void setIndegrees(List<Integer> indegrees) {
        this.indegrees = indegrees;
    }


}
