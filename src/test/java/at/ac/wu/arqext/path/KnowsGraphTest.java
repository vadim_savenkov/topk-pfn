package at.ac.wu.arqext.path;

import at.ac.wu.arqext.TestUtil;
import at.ac.wu.graphsense.hdt.HDTNodeGraphIndex;
import org.apache.jena.graph.Graph;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.pfunction.PropertyFunctionRegistry;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.apache.jena.rdf.model.Model;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdtjena.HDTGraph;

/**
 * Created by vadim on 10.05.17.
 */
public class KnowsGraphTest {

    final static String knowsGraph ="@prefix : <http://dbpedia.org/resource/> . "
            + "@prefix db: <http://dbpedia.org/resource/> ."
            + ":a :knows :b . :b :knows :c . :c :name \"qaiser\" ."
            + ":a :knows :e . :e :knows :c . :e :name \"mehmood\" ";

    Model model;

    final static String queries[] = {
        "PREFIX : <http://dbpedia.org/resource/>\n"
        + "PREFIX ppf: <"+ PathPropertyFunctionFactory.NAMESPACE+">\n"
        + "SELECT * "
        + "WHERE { ?x :knows ?y . ?y :knows ?z . ?path ppf:topk (?x ?z 2 \"(:knows*)\") }"

        ,"PREFIX : <http://dbpedia.org/resource/>\n"
        + "PREFIX ppfj: <java:at.ac.wu.arqext.path.>\n"
        + "SELECT * "
        + "WHERE { ?x :knows ?y . ?y :knows ?z . ?path ppfj:topk (?x ?z 2 \"(:knows*)\") }"

        ,"PREFIX : <http://dbpedia.org/resource/>\n"
        + "PREFIX ppf: <"+ PathPropertyFunctionFactory.NAMESPACE+">\n"
        + "PREFIX ppfj: <java:at.ac.wu.arqext.path.>\n"
        + "SELECT * "
        + "WHERE { ?x :knows ?y . ?y :knows ?z . ?p1 ppf:topk (?x ?z 2 \"(:knows*)\") . ?p2 ppfj:topk (?x ?z 2) }"

        ,"PREFIX : <http://dbpedia.org/resource/>\n"
        + "PREFIX ppf: <"+ PathPropertyFunctionFactory.NAMESPACE+">\n"
        + "PREFIX ppfj: <java:at.ac.wu.arqext.path.>\n"
        + "SELECT DISTINCT ?p1 ?p2 "
        + "WHERE { ?x :knows ?y . ?y :knows ?z . ?p1 ppf:topk (?x ?z 2 \"(:knows*)\") . ?p2 ppfj:topk (?x ?z 2) }"

        ,"PREFIX : <http://dbpedia.org/resource/>\n"
        + "PREFIX ppf: <"+ PathPropertyFunctionFactory.NAMESPACE+">\n"
        + "PREFIX ppfj: <java:at.ac.wu.arqext.path.>\n"
        + "SELECT DISTINCT ?p1 ?p2 "
        + "WHERE { ?x :knows ?y . ?y :knows ?z . ?p1 ppf:topk (\"--source\" ?x \"--target\" ?z \"--k\" 2 \"--pattern\" \"(:knows*)\") . ?p2 ppfj:topk (?x ?z 2) }"
    };

    @BeforeClass
    static public void setupStatic(){
        PropertyFunctionRegistry reg = PropertyFunctionRegistry.chooseRegistry(ARQ.getContext());
        reg.put(topk.URI, new PathPropertyFunctionFactory());
    }

    public void setupHDT(){
        HDT hdt = TestUtil.createHDT(TestUtil.createLabeledGraph(knowsGraph));
        Graph graph = new HDTGraph(hdt, true);
        this.model = ModelFactory.createModelForGraph(graph);

    }

    @Before
    public void setupRDF(){
        this.model = TestUtil.createLabeledGraph(knowsGraph);
    }


    public void setupHDTDBpedia(){
        HDTNodeGraphIndex gi = new HDTNodeGraphIndex( DBpediaTest.getDatasetPath(), true );

        this.model = ModelFactory.createModelForGraph(gi.getGraph());
    }


    @Test
    public void test_queries(){
        for(int i=0; i<queries.length; i++){
            System.out.println("QUERY #"+(i+1));

            Query query = QueryFactory.create(queries[i]) ;
            try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {

                ResultSet results = qexec.execSelect() ;
                for ( ; results.hasNext() ; ) {

                    QuerySolution soln = results.nextSolution();
                    StringBuilder sb = new StringBuilder();

                    for (Var v : query.getProjectVars() ) {
                            sb.append(v.toString());
                            sb.append("=");

                            RDFNode val = soln.get(v.getVarName());
                            sb.append(val==null? "(nil)" : "("+val.toString()+")");
                    }
                    System.out.println(sb);
                }
            }

        }
    }


}
