package at.ac.wu.arqext.path;

import at.ac.wu.arqext.QueryLogRecord;
import com.j256.ormlite.stmt.*;
import org.junit.*;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by Vadim on 11.06.2017.
 */
public class Repeater {

    final int REPEAT_THRESHOLD = 3600000;

    @BeforeClass
    public static void setup(){
        QueryLogRecord.setup();
        DBpediaTest.setupStatic();
    }


    @AfterClass
    public static void tearDown(){
        QueryLogRecord.shutdown();
    }



    public void changeIDs(){
        try {
            List<QueryLogRecord> records = QueryLogRecord.dao.queryForAll();

            for( QueryLogRecord r : records ){
                if( r.getExperiment().contains("challenge")  ){
                    //QueryLogRecord.dao.delete(r);
                }
            }
        }
        catch( Exception ex ){
            Assert.fail();
        }
    }

    @Test
    public void repeatOnThisMachine(){

        boolean otherMachines = true;
        boolean repeatSuccessful = false;

        int doSkip = 0;

        String hostName = QueryLogRecord.lookupHostname();
        Date ts = new Date();
        if( hostName.isEmpty() ){
            Assert.fail();
        }
        try {
            int numConditions = 3;

            QueryBuilder<QueryLogRecord, String> queryBuilder = QueryLogRecord.dao.queryBuilder();
            Where<QueryLogRecord, String> where = queryBuilder.where();
            if( otherMachines ) {
                where.notIn("hostname", hostName);
            }
            else{
                where.in("hostname",hostName);
            }
            File fileDataset = new File(DBpediaTest.getDatasetPath());
            where.eq("dataset",fileDataset.getName());
            if( repeatSuccessful ) {
                where.lt("runtimeMillis", REPEAT_THRESHOLD); //only take successfully terminated queries
            }
            else {
                where.isNull("runtimeMillis");
                where.ge("runtimeMillis", REPEAT_THRESHOLD);
                where.or(2);
            }

            where.and(numConditions); // applies to the preceding n conditions

            PreparedQuery<QueryLogRecord> preparedQuery = queryBuilder.prepare();

            List<QueryLogRecord> records = QueryLogRecord.dao.query(preparedQuery);

            int skipped = 0;
            for( QueryLogRecord r : records ){

                if( skipped++<doSkip ){
                    continue;
                }

                r.setTs(ts);

                if( r.getQueryText()!= null && !r.getQueryText().isEmpty() ){
                    DBpediaTest.runQuery(r, r.getQueryText());
                }
                else{
                    DBpediaTest.runSinglePathQuery( r );
                }
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
            Assert.fail();
        }

    }


    @Test
    public void scaleK(){

        double scale = 2;

        int nRepeat = 100;

        String hostName = QueryLogRecord.lookupHostname();
        Date ts = new Date();
        if( hostName.isEmpty() ){
            Assert.fail();
        }
        try {

            for( int i=0; i<nRepeat; i++ ) {

                StringBuilder sb = new StringBuilder("SELECT \"id\" FROM \"QUERYLOGRECORD\" Q WHERE ");

                sb.append(" \"hostname\"='"+hostName+"'");

                File fileDataset = new File(DBpediaTest.getDatasetPath());
                sb.append(" AND \"dataset\"='"+fileDataset.getName()+"'");
                sb.append(" AND \"runtimeMillis\" < "+REPEAT_THRESHOLD);
                sb.append(" AND \"experiment\"='incrTest'");

                sb.append(" AND NOT EXISTS(SELECT 1 FROM \"QUERYLOGRECORD\" QL WHERE QL.\"dataset\"=Q.\"dataset\" AND " +
                        " QL.\"experiment\" = Q.\"experiment\" AND QL.\"hostname\" = Q.\"hostname\" AND " +
                        " QL.\"source\" = Q.\"source\" AND QL.\"target\" = Q.\"target\" AND " +
                        " (Q.\"pathExpr\" IS NULL OR QL.\"pathExpr\" = Q.\"pathExpr\") AND " +
                        " (QL.\"runtimeMillis\" >= "+REPEAT_THRESHOLD+" OR QL.\"runtimeMillis\" IS NULL) )");

                sb.append(" AND NOT EXISTS(SELECT 1 FROM \"QUERYLOGRECORD\" QLR WHERE QLR.\"dataset\"=Q.\"dataset\" AND " +
                        " QLR.\"experiment\" = Q.\"experiment\" AND QLR.\"hostname\" = Q.\"hostname\" AND " +
                        " QLR.\"source\" = Q.\"source\" AND QLR.\"target\" = Q.\"target\" AND " +
                        " (Q.\"pathExpr\" IS NULL OR QLR.\"pathExpr\" = Q.\"pathExpr\") AND " +
                        " (QLR.\"desiredLength\" <= QLR.\"maxPathLength\") )");

                sb.append(" AND \"desiredLength\" > \"maxPathLength\"");
                sb.append(" ORDER BY \"k\" DESC, \"id\" DESC");

                List<String[]> ids = QueryLogRecord.dao.queryRaw(sb.toString()).getResults();

                if( !ids.isEmpty() ) {

                    QueryBuilder<QueryLogRecord, String> qb = QueryLogRecord.dao.queryBuilder();
                    qb.where().eq("id", ids.iterator().next()[0]);

                    List<QueryLogRecord> records = QueryLogRecord.dao.query(qb.prepare());

                    if (!records.isEmpty()) {

                        QueryLogRecord r = records.iterator().next();
                        r.setK((int) (r.getK() * scale));
                        r.setTs(ts);
                        DBpediaTest.runSinglePathQuery(r);
                    }
                }
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
            Assert.fail();
        }

    }

}
