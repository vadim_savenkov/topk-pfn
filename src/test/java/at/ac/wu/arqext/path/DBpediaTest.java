package at.ac.wu.arqext.path;

import at.ac.wu.arqext.PathStatistics;
import at.ac.wu.arqext.QueryLogRecord;
import at.ac.wu.graphsense.Edge;
import at.ac.wu.graphsense.Util;
import at.ac.wu.graphsense.hdt.HDTNodeGraphIndex;
import at.ac.wu.graphsense.rdf.RDFGraphIndex;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.*;

import java.io.File;
import java.util.*;

import org.apache.jena.graph.Graph;
import org.apache.jena.query.*;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.pfunction.PropertyFunctionRegistry;
import org.junit.*;
import org.junit.rules.TestName;
import org.rdfhdt.hdt.enums.TripleComponentRole;
import org.rdfhdt.hdtjena.HDTGraph;


/**
 * Created by vadim on 07.06.17.
 */
@Ignore
public class DBpediaTest {

    static Model model;
    static Graph graph;
    static RDFGraphIndex gix;

    Date ts = null;

    @Rule public TestName name = new TestName();


    final static String _datasetPath = "training_dataset.hdt";

    public final static String PREFIX = "PREFIX : <http://dbpedia.org/property/>\n"
            + "PREFIX dbr: <http://dbpedia.org/resource/>\n"
            + "PREFIX dbo: <http://dbpedia.org/ontology/>\n"
            + "PREFIX dbp: <http://dbpedia.org/property/>\n"
            //+ "PREFIX ppf: <"+ PathPropertyFunctionFactory.NAMESPACE+">\n"
            + "PREFIX ppf: <java:at.ac.wu.arqext.path.>\n"
            ;

    final static String queries[] = {
          
    		PREFIX + "SELECT * "
            + "WHERE { VALUES(?s ?o){(dbr:1952_Winter_Olympics dbr:Elliot_Richardson)} }"
            ,
            PREFIX + "SELECT * "
                    + "WHERE { VALUES(?s ?o){(dbr:1952_Winter_Olympics dbr:Elliot_Richardson)}" +
                    ". ?path ppf:topk (?s ?o 2) }"
            ,
            PREFIX + "SELECT * "
            + "WHERE { VALUES(?s ?o){(dbr:1952_Winter_Olympics dbr:Elliot_Richardson)}" +
                    ". ?path ppf:topk (?s ?o 2 \"(:after/:after/:officiallyOpenedBy/:defense)*\") }"
            ,
            
            PREFIX + "SELECT * "
                    + "WHERE { VALUES(?s ?o){(dbr:1952_Winter_Olympics dbr:Elliot_Richardson)}" +
                            ". ?path ppf:topk (?s ?o 2 \"(:after/:after/:officiallyOpenedBy/!(:defense))\") }"
            ,
            
            PREFIX + "SELECT * "
                            + "WHERE { VALUES(?s ?o){(dbr:1952_Winter_Olympics dbr:Elliot_Richardson)}" +
                                    ". ?path ppf:topk (?s ?o 2 \"(:after/:after*/:officiallyOpenedBy/!(:defense))\") }"
                            
      		,

            PREFIX + "SELECT * "
                + "WHERE { VALUES(?s ?o){(dbr:1992_Spanish_Grand_Prix dbr:Michael_Schumacher)}" +
                ". ?path ppf:topk (?s ?o 1) }"

            ,
      		PREFIX + "SELECT * "
                      + "WHERE { VALUES(?s ?o){(dbr:1952_Winter_Olympics dbr:Elliot_Richardson)}" +
                      ". ?path ppf:topk (?s ?o 2 \"((:after/:after/:officiallyOpenedBy/:defense ) | (:after/:after/:officiallyOpenedBy/!:justice))\") }"
                      
    };

    static int desiredLength = 15;

    static boolean uniqueResources = false;

    static int k = 5;

    static String getDatasetPath(){
        return System.getProperty("dataset", _datasetPath);
    }


    @BeforeClass
    static public void setupStatic(){
        PropertyFunctionRegistry reg = PropertyFunctionRegistry.chooseRegistry(ARQ.getContext());
        reg.put(topk.URI, new PathPropertyFunctionFactory());

        gix = new HDTNodeGraphIndex(getDatasetPath(), true); //false: non-indexed (default) | true : indexed

        graph = gix.getGraph();
        model = ModelFactory.createModelForGraph(graph);

        long nTriples = ((HDTGraph)gix.getGraph()).getHDT().getTriples().getNumberOfElements();
        long nPredicates = ((HDTGraph)gix.getGraph()).getHDT().getDictionary().getNpredicates();
        long nSubjects = ((HDTGraph)gix.getGraph()).getHDT().getDictionary().getNsubjects();
        long nObjects = ((HDTGraph)gix.getGraph()).getHDT().getDictionary().getNobjects();
        long nShared = ((HDTGraph)gix.getGraph()).getHDT().getDictionary().getNshared();

        System.out.println(
                String.format("#Triples %d #Preidcates %d #Subjects %d #Objects %d #Shared %d"
                        ,nTriples,nPredicates,nSubjects,nObjects,nShared));

        QueryLogRecord.setup();

    }

    @AfterClass
    public static void tearDown(){
       QueryLogRecord.shutdown();
    }


    @Before
    public void setupDataset(){
        ts = new Date();
    }

    @Test
    public void dummy(){

    }

    @Test
    public void test_queries(){
        for(int i=0; i<queries.length; i++){
            System.out.println("QUERY #"+(i+1));

            Query query = QueryFactory.create(queries[i]) ;
            try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {

                ResultSet results = qexec.execSelect() ;
                while( results.hasNext() ) {

                    QuerySolution soln = results.nextSolution();
                    StringBuilder sb = new StringBuilder();

                    for (Var v : query.getProjectVars() ) {
                        sb.append(v.toString());
                        sb.append("=");

                        RDFNode val = soln.get(v.getVarName());
                        sb.append(val==null? "(nil)" : "("+val.toString()+")");
                    }
                    System.out.println(sb);
                }
            }

        }
    }


    @Test
    public void challengeESWC16(){

        List<String> testcases = new LinkedList<>();

        testcases.add("task1_q1_8,8,dbr:Felipe_Massa,dbr:Red_Bull,no");
        testcases.add("task1_q1_344,344,dbr:Felipe_Massa,dbr:Red_Bull,no");
        testcases.add("task1_q1_1068,1068,dbr:Felipe_Massa,dbr:Red_Bull,no");
        testcases.add("task1_q1_20152,20152,dbr:Felipe_Massa,dbr:Red_Bull,no");

        testcases.add("task1_q2_3,3,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,no");
        testcases.add("task1_q2_4,4,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,no");
        testcases.add("task1_q2_79,79,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,no");
        testcases.add("task1_q2_154,154,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,no");
        testcases.add("task1_eval_q2_377,377,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,no");
        testcases.add("task1_eval_q2_53008,53008,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,no");

        testcases.add("task1_q3_36,36,dbr:Karl_W._Hofmann,dbr:Elliot_Richardson,no");
        testcases.add("task1_q3_336,336,dbr:Karl_W._Hofmann,dbr:Elliot_Richardson,no");
        testcases.add("task1_q3_4866,4866,dbr:Karl_W._Hofmann,dbr:Elliot_Richardson,no");

        testcases.add("task1_q4_2,2,dbr:James_K._Polk,dbr:Felix_Grundy,no");
        testcases.add("task1_q4_16,16,dbr:James_K._Polk,dbr:Felix_Grundy,no");
        testcases.add("task1_q4_250,250,dbr:James_K._Polk,dbr:Felix_Grundy,no");

        testcases.add("task1_q4_1906,1906,dbr:James_K._Polk,dbr:Felix_Grundy,no");
        testcases.add("task1_q4_20224,20224,dbr:James_K._Polk,dbr:Felix_Grundy,no");
        testcases.add("task1_q4_175560,175560,dbr:James_K._Polk,dbr:Felix_Grundy,no");

        testcases.add("task2_q1_32,32,dbr:Felipe_Massa,dbr:Red_Bull,dbp:firstWin");
        testcases.add("task2_q1_98,98,dbr:Felipe_Massa,dbr:Red_Bull,dbp:firstWin");
        testcases.add("task2_q1_1914,1914,dbr:Felipe_Massa,dbr:Red_Bull,dbp:firstWin");
        testcases.add("task2_q1_16632,16632,dbr:Felipe_Massa,dbr:Red_Bull,dbp:firstWin");
        testcases.add("task2_q1_212988,212988,dbr:Felipe_Massa,dbr:Red_Bull,dbp:firstWin");

        testcases.add("task2_q2_3,3,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,dbp:after");
        testcases.add("task2_q2_4,4,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,dbp:after");
        testcases.add("task2_q2_76,76,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,dbp:after");
        testcases.add("task2_q2_151,151,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,dbp:after");
        testcases.add("task2_q2_2311,2311,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,dbp:after");
        testcases.add("task2_eval_q2_374,374,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,dbp:after");
        testcases.add("task2_eval_q2_52664,52664,dbr:1952_Winter_Olympics,dbr:Elliot_Richardson,dbp:after");


        testcases.add("task2_q3_12,12,dbr:Karl_W._Hofmann,dbr:Elliot_Richardson,dbp:predecessor");
        testcases.add("task2_q3_76,76,dbr:Karl_W._Hofmann,dbr:Elliot_Richardson,dbp:predecessor");
        testcases.add("task2_q3_1440,1440,dbr:Karl_W._Hofmann,dbr:Elliot_Richardson,dbp:predecessor");
        testcases.add("task2_q3_8088,8088,dbr:Karl_W._Hofmann,dbr:Elliot_Richardson,dbp:predecessor");

        testcases.add("task2_q4_1,1,dbr:James_K._Polk,dbr:Felix_Grundy,dbo:president");
        testcases.add("task2_q4_6,6,dbr:James_K._Polk,dbr:Felix_Grundy,dbo:president");

        testcases.add("task2_q4_72,72,dbr:James_K._Polk,dbr:Felix_Grundy,dbo:president");
        testcases.add("task2_q4_614,614,dbr:James_K._Polk,dbr:Felix_Grundy,dbo:president");
        testcases.add("task2_q4_5483,5483,dbr:James_K._Polk,dbr:Felix_Grundy,dbo:president");
        testcases.add("task2_q4_52649,52649,dbr:James_K._Polk,dbr:Felix_Grundy,dbo:president");
        testcases.add("task2_q4_471199,471199,dbr:James_K._Polk,dbr:Felix_Grundy,dbo:president");

        for( String q : testcases ){
            String task[] = q.split(",");
            QueryLogRecord r = makeRecord();
            r.setQueryId(task[0]);
            r.setK( Integer.valueOf(task[1]) );
            r.setSource( task[2] );
            r.setTarget( task[3] );

            if( !task[4].equals("no") ){
                r.setPathExpr( task[4]+"/!:*|!:*/" + task[4] );
            }

            // sometimes it makes sense not to log first (cold) results
            // so the wrieLog parameter can eb someting like "task[0].contains("t1_") && task[0].contains("_q2_")"
            runSinglePathQuery(r, true );
        }

    }

    @Test
    public void incrTest(){

        desiredLength = 35;
        uniqueResources = false;
        k = 20;

        for( int length = 2; length <= desiredLength; length+=2 ) {

            PathStatistics<Node,Node> ps = randomPath(length);

            QueryLogRecord r = makeRecord();

            r.setPathStatistics(ps);
            r.setK(k);

            searchWithPrototype(r);

            System.out.println( String.format( "L: %d k: %d found %d",
                                ps.length(), r.getK(), r.getPathsFound() ) );
        }

    }

    private static void searchWithPrototype( QueryLogRecord r ){
        searchWithPrototype(r, 0);
    }

    private static void searchWithPrototype( QueryLogRecord r, int derivePatterns ){
        List<Edge<Node,Node>> path = r.getPathStatistics().getPath();

        String  source = path.get(0).vertex().getURI()
               ,target = path.get(path.size()-1).vertex().getURI();
        r.setSource(source);
        r.setTarget(target);

        String queryString =
                String.format(
                PREFIX + "SELECT ?path "
                + "WHERE { ?path ppf:topk (<%s> <%s> %d) }"
                        , source //can't get r here since URIs are already compressed for readability of logging there.
                        , target
                        , r.getK());

        System.out.println(queryString);
        runQuery( r, queryString );
    }

    public static void runSinglePathQuery( QueryLogRecord r ){ runSinglePathQuery(r, true); }

    public static void runSinglePathQuery( QueryLogRecord r, boolean writeLog ){
        String queryText = "";
        if(r.getPathExpr()!=null) {
            queryText = String.format("SELECT ?path WHERE { ?path ppf:topk (%s %s %d '%s')}"
                    , QueryLogRecord.expandURI(r.getSource())
                    , QueryLogRecord.expandURI(r.getTarget()), r.getK(), r.getPathExpr());
        }
        else{
            queryText = String.format("SELECT ?path WHERE { ?path ppf:topk (%s %s %d)}"
                    , QueryLogRecord.expandURI(r.getSource())
                    , QueryLogRecord.expandURI(r.getTarget()), r.getK());


        }
        System.out.println(queryText);

        r.setHostname(QueryLogRecord.lookupHostname());

        runQuery(r, PREFIX+queryText, writeLog);
    }

    public static void runQuery ( QueryLogRecord r, String queryText ){ runQuery(r, queryText, true); }

    public static void runQuery( QueryLogRecord r, String queryText, boolean writeLog ){

        r.setQueryText(queryText);
        r.setHostname( QueryLogRecord.lookupHostname() );

        //for re-running experiments
        r.setMinPathLength(null);
        r.setMaxPathLength(null);
        r.setPathsFound(null);
        r.setRuntimeMillis(null);
        r.setShortestPath(null);
        r.setKthShortestPath(null);

        if( writeLog ) {
            QueryLogRecord.write(r, false);
        }

        Query query = QueryFactory.create(queryText) ;
        try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {

            r.resetCreationTime();
            ResultSet results = qexec.execSelect();
            QuerySolution first = null, last = null;
            int n = 0;

            while (results.hasNext()) {
                QuerySolution s = results.next();
                if (first == null) {
                    first = s;
                }
                if (!results.hasNext()) {
                    last = s;
                }
                n++;
            }
            r.setPathsFound(n);
            if( first!=null && first.contains("path") ) {
                String strPath = first.getLiteral("path").toString();
                r.setMinPathLength(PathStatistics.lengthFromStringTrace(strPath));
                r.setShortestPath(strPath);
            }
            if( last!=null && last.contains("path") ) {
                String strPath = last.getLiteral("path").toString();
                r.setMaxPathLength(PathStatistics.lengthFromStringTrace(strPath));
                r.setKthShortestPath(strPath);
            }
        }

        if( writeLog ) {
            QueryLogRecord.write(r);
        }

    }


    int trialCount = 0;

    private PathStatistics<Node,Node> randomPathFromSparql( int length ){

        Node subject=null, object =null;

        Random rand = new Random();
        if( (graph instanceof HDTGraph ) ) {
            int range = (int) ((HDTGraph) graph).getHDT().getDictionary().getNshared();

            int ntry = 0;
            while (subject == null && ntry++ < 1000) {
                String uri = ((HDTGraph) graph).getHDT().getDictionary().
                        idToString(rand.nextInt(range), TripleComponentRole.SUBJECT).toString();
                if (uri != null && uri.startsWith("http")) {
                    subject = ResourceFactory.createResource(uri).asNode();
                }
            }

            while (object == null && ntry++ < 1000) {
                String uri = ((HDTGraph) graph).getHDT().getDictionary().
                        idToString(rand.nextInt(range), TripleComponentRole.SUBJECT).toString();

                if (uri != null && uri.startsWith("http") ){
                    Node tryO = ResourceFactory.createResource(uri).asNode();
                    if( !subject.equals(tryO)) {
                        object = tryO;
                    }
                }
            }
        }

        List<Edge<Node,Node>> path = new LinkedList<>();
        path.add(new Edge<Node,Node>(subject,null));
        path.add(new Edge<>(object,NodeFactory.createURI("http://d")));
        return new PathStatistics<>(path);
    }

    private  PathStatistics<Node,Node> randomPath( int length ){

        if( trialCount++ > 1000 ){
            Assert.fail();
            throw new RuntimeException("can't find prototype path of length " + length);
        }

        Random rand = new Random();

        Resource subject = null;

        if( (graph instanceof HDTGraph ) ) {
            int range = (int)((HDTGraph)graph).getHDT().getDictionary().getNshared();

            int ntry = 0;
            while(subject == null && ntry++<1000 ){
                String uri = ((HDTGraph)graph).getHDT().getDictionary().
                        idToString(rand.nextInt(range), TripleComponentRole.SUBJECT).toString();
                if( uri != null && uri.startsWith("http") ){
                    subject = ResourceFactory.createResource(uri);
                }
            }
        }
        else {
            String sq =  "select * where { ?o0 ?p1 ?o1. ";
            StringBuilder f = new StringBuilder("filter(1=1 ");

            for(int i=1;i<length; i++){
                sq += String.format( "?o%1$d ?p%2$d ?o%2$d.", i, i+1 );

                for(int j = i-1; j>=0; j--){
                    f.append(" && (?o");f.append(j); f.append("!=?o"); f.append(i);
                    f.append(" || ?p");f.append(i); f.append("!=?p"); f.append(i+1);
                    f.append(" || ?o");f.append(i); f.append("!=?o"); f.append(i+1);
                    f.append(") ");
                }
            }
            sq += f.toString() + ")} offset %rand limit 1";

            System.out.println(sq);
            int range = 10;

            Query query = QueryFactory.create(sq.replace("%rand", Integer.toString(rand.nextInt(range))));

            try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {

                ResultSet results = qexec.execSelect();
                if (results.hasNext()) {
                    QuerySolution sol = results.next();

                    List<Edge<Node,Node>> path = new LinkedList<>();

                    path.add(new Edge<Node, Node>(sol.getResource("s").asNode(),null));

                    for(int i=1;i<length; i++){
                        String p = "p"+i, o = "o"+i;
                        path.add(new Edge<>(sol.get(o).asNode(),sol.get(p).asNode()));
                    }
                    System.out.println(Util.format(path));
                    return  new PathStatistics<>(path);
                }
            }
        }

        if( subject == null ){
            return randomPath(length);
        }

        LinkedList<Triple> triplePath = new LinkedList<>();
        LinkedList<Integer> indegrees = new LinkedList<>();
        LinkedList<Integer> outdegrees = new LinkedList<>();

        Node subjectNode = subject.asNode();
        Set<Node> usedResources = new HashSet<>();
        usedResources.add(subjectNode);


        while( triplePath.size() < length ) {
            List<Triple> ts = graph.find(null, null, subjectNode).toList();
            Collections.shuffle(ts, rand);

            Triple next = null;
            int indegree = 0;

            for( Triple t : ts ){
                indegree++;
                if ( next == null )
                {
                    if( !usedResources.contains(t.getSubject())
                       || (!uniqueResources && !triplePath.contains(t)) )
                    { //
                        next = t;
                        triplePath.push(t);

                        int outdegree = 0;
                        Iterator<Triple> other = graph.find(t.getSubject(), null, null);
                        while (other.hasNext()) {
                            Triple tOut = other.next();
                            if (tOut.getObject().isURI()) {
                                outdegree++;
                            }
                        }
                        if (outdegree == 0) {
                            Assert.fail();
                        }
                        outdegrees.push(outdegree);

                        subjectNode = t.getSubject();
                        usedResources.add(subjectNode);
                    }
                }
            }

            if( next == null ){
                if( ((double)(length - triplePath.size()))/length < 0.1 ){
                    System.out.println("Stopping early, at "+triplePath.size()+" instead of "+length);
                    break;
                }

                //restart from different node
                return randomPath(length);
            }

            indegrees.push(indegree);
        }


        List<Edge<Node,Node>> path = new LinkedList<>();
        path.add(new Edge<Node, Node>(triplePath.getFirst().getSubject(),null));

        for(Triple t : triplePath){
            path.add(new Edge<>(t.getObject(),t.getPredicate()));
        }

        PathStatistics<Node,Node> ps = new PathStatistics<>(path);
        ps.setIndegrees(indegrees);
        ps.setOutdegrees( outdegrees );

        return ps;
    }



    public QueryLogRecord makeRecord(){
        QueryLogRecord r = new QueryLogRecord();
        File fileDataset = new File(getDatasetPath());
        r.setDataset(fileDataset.getName());

        r.setTs(ts);

        r.setEdgeInt(fileDataset.getName().endsWith(".hdt") &&
                (System.getProperty("nohdt")==null || !System.getProperty("nohdt").equals("true")));
        r.setExperiment(name.getMethodName());
        r.setDesiredLength( this.desiredLength );
        r.setPpUniqueResources( this.uniqueResources );
        return r;
    }

}
